document.addEventListener('DOMContentLoaded', function () {
  const bookList = document.getElementById('book-list');
  const modal = document.getElementById('modal');
  const modalImage = document.getElementById('modal-image');
  const modalTitle = document.getElementById('modal-title');
  const modalAuthor = document.getElementById('modal-author');
  const modalDescription = document.getElementById('modal-description');
  const modalPages = document.getElementById('modal-pages');
  const buyLink = document.getElementById('buy-link');
  const favoriteButton = document.getElementById('favorite-button');
  const closeButton = document.querySelector('.close-button');

  fetch('books.json')
    .then((response) => response.json())
    .then((books) => {
      books.forEach((book) => {
        const bookDiv = document.createElement('div');
        bookDiv.classList.add('book');
        bookDiv.setAttribute('data-id', book.id);
        bookDiv.innerHTML = `
                    <img src="${book.image}" alt="${book.title}">
                    <h2>${book.title}</h2>
                    <p>${book.author}</p>`;
        bookList.appendChild(bookDiv);

        bookDiv.addEventListener('click', () => {
          modalImage.src = book.image;
          modalTitle.textContent = book.title;
          modalAuthor.textContent = 'Author: ' + book.author;
          modalDescription.textContent = 'Description: ' + book.description;
          modalPages.textContent = 'Pages: ' + book.pages;
          buyLink.href = book.buyLink;
          modal.style.display = 'block';

          const isFavorite =
            localStorage.getItem(`favorite-${book.id}`) === 'true';
          setFavoriteState(isFavorite);
          modal.dataset.bookId = book.id;
        });
      });
    });

  closeButton.addEventListener('click', () => {
    modal.style.display = 'none';
  });

  window.addEventListener('click', (event) => {
    if (event.target == modal) {
      modal.style.display = 'none';
    }
  });

  favoriteButton.addEventListener('click', function () {
    const isFavorite = favoriteButton.dataset.favorite === 'true';
    const bookId = modal.dataset.bookId;

    if (!isFavorite) {
      setFavoriteState(true);
      localStorage.setItem(`favorite-${bookId}`, 'true');
    } else {
      setFavoriteState(false);
      localStorage.setItem(`favorite-${bookId}`, 'false');
    }
  });

  function setFavoriteState(isFavorite) {
    if (isFavorite) {
      favoriteButton.dataset.favorite = 'true';
      favoriteButton.textContent = 'Remove from Favorites';
      favoriteButton.style.backgroundColor = '#DC143C';
      favoriteButton.style.color = 'white';
    } else {
      favoriteButton.dataset.favorite = 'false';
      favoriteButton.textContent = 'Add to Favorites';
      favoriteButton.style.backgroundColor = '#FF6666';
      favoriteButton.style.color = 'black';
    }
  }
});
